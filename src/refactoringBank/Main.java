package refactoringBank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс мини Банк
 */

public class Main {

    public static final Account ACCOUNT_VTB = new Account("1234234534564567", "Tatiana Iniushkina", 1000);
    public static final Account ACCOUNT_SBERBANK = new Account("2234234534564567", "Tatiana Iniushkina", 1000);

    public static void main(String[] args) {
        final Account.Card maestro = ACCOUNT_VTB.new Card("3340 7845 8956 8906", "Маестро(ВТБ)");
        final Account.Card mir = ACCOUNT_SBERBANK.new Card("3340 7845 8956 8906", "Мир(Сбербанк)");
        final Account.Card mastercard = ACCOUNT_SBERBANK.new Card("3340 7845 8956 8906", "Мастеркарт(Сбербанк)");
        ArrayList<Account.Card> arrayCard = new ArrayList(Arrays.asList(maestro, mir, mastercard));
        choice(arrayCard);
    }

    /**
     * Метод,который анализирует выбор пользователя и вызывает метод для выполнения операции
     * @param arrayCard -динамический массив со всеми картами Банков
     */

    public static void choice(ArrayList<Account.Card> arrayCard) {
        Scanner sc = new Scanner(System.in);
        int selecting = 0;
        do {
            printBalance();
            System.out.println("Что вы хотите сделать? \n 0.Ничего \n 1.Положить деньги \n 2.Вывести деньги");
            selecting = sc.nextInt();
            if (selecting < 0 && selecting > 2) {
                System.out.println("Выберите карту: ");
                for (int i = 0; i < arrayCard.size(); i++) {
                    System.out.println(arrayCard.get(i).getService());
                }
                int numberCard = sc.nextInt() - 1;
                operation(arrayCard.get(numberCard), selecting);
            }
        } while (selecting > 0 && selecting < 3);
    }

    /**
     * Метод для выполнения выбранной операции,над выбранной картой пользователя
     * @param card выбранная карта
     * @param selecting выбор операции
     */

    public static void operation(Account.Card card, int selecting) {
        System.out.println("Какую сумму?");
        Scanner sc = new Scanner(System.in);
        int amount = sc.nextInt();
        if (selecting == 1) {
            if (errorResponse(card.put(amount)) == 1) {
            }
        }
        if (selecting == 2) {
            if (errorResponse(card.withdraw(amount)) == 1) {
            }
        }
    }

    /**
     * Метод для вывода баланса,на всех пользовательских аккаунтах
     */

    public static void printBalance() {
        System.out.println("Сумма ,которая на балансе Сбербанка: " + ACCOUNT_SBERBANK.getAmount());
        System.out.println("Сумма ,которая на балансе VTB: " + ACCOUNT_VTB.getAmount());
    }

    /**
     *
     * @param error код ошибки или возвращаемая сумма(в том случае,если снимаемая сумма больше баланса)
     * @return
     */

    private static int errorResponse(int error) {
        int num = 1;
        if (error == 1) {
            System.out.println("Ошибка!введите другую сумму!");
            return 0;
        }
        if (error != 1 && error != 0) {
            System.out.println("Ошибка!Такой суммы нет,бери что есть! Забирай и проваливай! *" + error + "р*");
            return 0;
        }
        return num;
    }
}