package refactoringBank;

public class Account {
    private final String number;
    private final String owner;
    private int amount;

    public Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    public Account(final String number, final String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    public int getAmount() {
        return amount;
    }

    /**
     * метод снятия денег с аккаунта
     * @param amountToWithdraw -сумма снятия
     * @return код ошибки или возвращаемая сумма(в том случае,если снимаемая сумма больше баланса)
     */

    public int withdraw(int amountToWithdraw) {
        int error = 0;
        if (amountToWithdraw < 0) {
            error = 1;
        }
        if (amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        amount = amount - amountToWithdraw;
        return error;
    }

    /**
     * метод пополнения счета
     * @param amountToPut сумма пополнения
     * @return код ошибки
     */

    public int put(int amountToPut) {
        int error = 0;
        if (amountToPut < 0) {
            return 1;
        }
        amount = amount + amountToPut;
        return error;
    }

    /**
     * Внутренний класс карт
     */

    public class Card {
        private final String number;
        private final String service;

        public Card(final String number, String service) {
            this.number = number;
            this.service = service;
        }

        public String getService() {
            return service;
        }

        public String getNumber() {
            return number;
        }

        public int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        public int put(final int amountToPut) {
            return Account.this.put(amountToPut);
        }
    }
}
